# Converterscript

Convertereinstellungen für Handbrake, damit Videos aus allen (un)möglichen Quellen auch ohne VLC mit dem Firmware-Video-Player lauffähig sind: https://handbrake.fr/

0. whiteboards.json herunterladen: 
  - dazu auf die Datei whiteboards.json klicken
  - auf "Originalformat" klicken
  - Maus in das Fenster mit dem reinen Quelltext setzen
  - Strg A (um alles im Browserfenster zu markieren)
  - Strg C (um das Markierte in die Zwischenablage zu kopieren)
  - Texteditor aufmachen (z.B. Notepad++, Kate, Mousepad etc. - keine Textverarbeitung, sondern einen reinen Texteditor!)
  - Strg V (um im Editor einzufügen)
1. in Handbrake die Einstellungen laden: Voreinstellungen - Importieren
2. Video "dateiname" damit konvertieren
3. konvertierte Videodatei in dateiname.mp4 umbenennen
4. auf'n Stick damit und 
5. davon dann abspielen

Hinweis: Das da hilft auch immer wieder weiter http://ytdl-org.github.io/youtube-dl/index.html
